const http = require('http');
const fs = require('fs');
const url = require('url');

module.exports = () => {
    const port = 8080
    const logPath = './log.json'
    const logObj = {}

    const writeLog = (logPath, logObj, res) => {
        fs.writeFile(logPath, JSON.stringify(logObj), function (err) {
            if (err) throw err;
            console.log('Log file was updated');
        });
    }

    fs.readFile(logPath, function (err, data) {
        if (err && err.code === 'ENOENT') {
            logObj.logs = [];
        } else if (err) {
            throw err;
        } else {
            logObj.logs = JSON.parse(data).logs;
        }
    });

    http.createServer(function (req, res) {
        const {
            query,
            pathname
        } = url.parse(req.url, true);
        const index = pathname.indexOf('/', 1);
        const route = index === -1 ? pathname : pathname.substring(0, index);
        const filename = index === -1 ? null : pathname.substring(index + 1);

        if (route === '/file') {
            if (req.method === 'POST') {
                let isNewFile = false;
                let message = '';
                if (!query.filename || !query.content) {
                    res.writeHead(400, {
                        'Content-Type': 'text/html'
                    });
                    res.end('Invalid file parameters');
                }
                fs.writeFile(query.filename, query.content, function (err) {
                    if (err && err.code === 'ENOENT') {
                        isNewFile = true;
                    } else if (err) throw err;
                    message = isNewFile ? `New file with name ${query.filename} saved` : `File with name ${query.filename} was updated`;
                    res.writeHead(200, {
                        'Content-Type': 'text/html'
                    });
                    res.end(message);
                    logObj.logs.push({
                        message: message,
                        time: Date.now()
                    })
                    writeLog(logPath, logObj, res);
                });
            }
            if (req.method === 'GET') {

                fs.readFile(filename, function (err, data) {
                    if (err && err.code === 'ENOENT') {
                        res.writeHead(400, {
                            'Content-Type': 'text/html'
                        });
                        res.end(`no files with ${filename} name found`);
                    } else if (err) {
                        throw err;
                    }
                    res.writeHead(200, {
                        'Content-Type': 'text/html'
                    });
                    res.end(data);
                });
                logObj.logs.push({
                    message: `file with name ${filename} was read`,
                    time: Date.now()
                })
                writeLog(logPath, logObj, res);
            }

        } else if (route === '/logs') {
            if (req.method === 'GET') {
                fs.readFile(logPath, function (err, data) {
                    if (err) {
                        throw err;
                    }
                    let logs = JSON.parse(data).logs;
                    if (query.from) {
                        logs = logs.filter(record => record.time >= parseInt(query.from));
                    }
                    if (query.to) {
                        logs = logs.filter(record => record.time < parseInt(query.to));
                    }
                    res.writeHead(200, {
                        'Content-Type': 'application/json'
                    });
                    res.end(JSON.stringify({
                        logs
                    }));
                });
            }
        } else {
            res.writeHead(404, {
                'Content-Type': 'text/html'
            });
            res.end(`resource ${route} not found`);
        }

    }).listen(process.env.PORT || port, () => {
        console.log(`Server is listening on port ${process.env.PORT || port}`);
    })
}